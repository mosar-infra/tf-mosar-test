module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.0.6"
  roles       = local.roles
  profiles    = local.profiles
  environment = var.environment
}
