# iam/locals
#
# Note: profiles and roles are matched by resource name e.g. "admin_frankfurt" So use the same ones where they shoud be matched!

locals {
  roles = {
    ecs_task = {
      role = {
        name               = "mosar-ecs-task-role-${var.environment}"
        description        = "role tasks"
        assume_role_policy = file("${path.root}/iam_specs/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "mosar-ecs-task-policy-${var.environment}"
        description = "Policy for task role"
        policy      = file("${path.root}/iam_specs/policy_ecs_task.json")
      }
      policy_attachment = {
        name = "mosar-ecs-task-policy-attachment-${var.environment}"
      }
    }
    ecs_task_execution = {
      role = {
        name               = "mosar-ecs-task-execution-role-${var.environment}"
        description        = "role to execute the ecs tasks"
        assume_role_policy = file("${path.root}/iam_specs/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "mosar-ecs-task-execution-policy-${var.environment}"
        description = "Policy to execute ecs tasks"
        policy      = file("${path.root}/iam_specs/policy_ecs_task_execution.json")
      }
      policy_attachment = {
        name = "mosar-ecs-task-execution-policy-attachment-${var.environment}"
      }
    } #,
    # admin_frankfurt        = {
    #   role                 = {
    #     name               = "admin_frankfurt"
    #     description        = "admin role for region eu central 1"
    #     assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ec2.json")
    #   }
    #   policy               = {
    #     name               = "admin_frankfurt_policy"
    #     description        = "Policy to all resources and actions in frankfurt"
    #     policy             = file("${path.root}/iam_spec_files/policy_admin_frankfurt.json")
    #   }
    #   policy_attachment    = {
    #     name               = "admin-frankfurt-policy-attachment"
    #   }
    #   profile              = {
    #     name               = "admin_frankfurt_profile"
    #   }
    # }
  }
}

locals {
  profiles = {
    # admin_frankfurt = {
    #   name = "admin_frankfurt_profile"
    # }
  }
}
