# root/locals.tf

locals {
  vpc_cidr = var.environment == "prod" ? "11.11.0.0/16" : "12.12.0.0/16"
}

# locals {
#   cidr_security_groups = {
#     internal = {
#       name        = "internal_sg"
#       description = "security group for internal private access"
#       ingress = {
#         gameservice = {
#           from        = 8080
#           to          = 8080
#           protocol    = "tcp"
#           cidr_blocks = module.networking.private_subnet_cidrs
#         }
#         flashcardservice = {
#           from        = 8181
#           to          = 8181
#           protocol    = "tcp"
#           cidr_blocks = module.networking.private_subnet_cidrs
#         }
#         mysql = {
#           from        = 3306
#           to          = 3306
#           protocol    = "tcp"
#           cidr_blocks = module.networking.private_subnet_cidrs
#         }
#       }
#       egress = {
#         http = {
#           from        = 0
#           to          = 0
#           protocol    = -1
#           cidr_blocks = ["0.0.0.0/0"]
#         }
#       }
#     }
#     dns = {
#       name        = "dns_sg"
#       description = "security group for dns routing traffic"
#       ingress = {
#         tcp = {
#           from        = 0
#           to          = 0
#           protocol    = -1
#           cidr_blocks = ["0.0.0.0/0"]
#         }
#       }
#       egress = {
#         http = {
#           from        = 0
#           to          = 0
#           protocol    = -1
#           cidr_blocks = ["0.0.0.0/0"]
#         }
#       }
#     }
#     public = {
#       name        = "public_sg"
#       description = "security group for public access"
#       ingress = {
#         https = {
#           from        = 443
#           to          = 443
#           protocol    = "tcp"
#           cidr_blocks = ["0.0.0.0/0"]
#         }
#       }
#     }
#   }
# }

# locals {
#   sg_security_groups = {
#     private-mosar = {
#       name        = "private_mosar_sg"
#       description = "security group for private access to mosar"
#       ingress = {
#         http = {
#           from            = 80
#           to              = 80
#           protocol        = "tcp"
#           security_groups = module.networking.public_security_groups.*.id
#         }
#         be = {
#           from            = 8080
#           to              = 8080
#           protocol        = "tcp"
#           security_groups = module.networking.public_security_groups.*.id
#         }
#       }
#       egress = {
#         http = {
#           from        = 0
#           to          = 0
#           protocol    = -1
#           cidr_blocks = ["0.0.0.0/0"]
#         }
#       }
#     }
#   }
# }

locals {
  lb_target_groups = {
    mosar = {
      port                = 80
      protocol            = "HTTP"
      path                = "/"
      target_type         = "ip"
      healthy_threshold   = 2
      unhealthy_threshold = 2
      timeout             = 3
      interval            = 30
    }
  }
}

locals {
  lb_tg_attachments = {
    # mosar = {
    #   port = 80
    # }
  }
}

locals {
  lb_ec2_tg_attachments = {}
}


locals {
  iam = {
    ecs_task = {
      role = {
        name               = "ecs-task-role"
        description        = "role tasks"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "ecs-task-policy"
        description = "Policy for task role"
        policy      = file("${path.root}/iam_spec_files/policy_ecs_task.json")
      }
      policy_attachment = {
        name = "ecs-task-policy-attachment"
      }
      profile = {
        name = "esc_task_profile"
      }
    }
    ecs_task_execution = {
      role = {
        name               = "ecs-task-execution-role"
        description        = "role to execute the ecs tasks"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "ecs-task-execution-policy"
        description = "Policy to execute ecs tasks"
        policy      = file("${path.root}/iam_spec_files/policy_ecs_task_execution.json")
      }
      policy_attachment = {
        name = "ecs-task-execution-policy-attachment"
      }
      profile = {
        name = "esc_task_execution_profile"
      }
    }
  }
}

# locals {
#   container_spec_definitions = {
#     flashcards = {
#       name              = "flashcards"
#       desired_count     = 1
#       spec_file         = file("${path.root}/container_spec_files/mosar-flashcards-container.tpl")
#       subnet_ids        = module.networking.private_subnet.*.id
#       security_groups   = concat(module.networking.private_security_groups["private-mosar"].*.id, module.networking.internal_security_groups.*.id, module.networking.cidr_security_groups["dns"].*.id)
#       family            = "flashcards-family"
#       use_load_balancer = false
#     }
#     game = {
#       name              = "game"
#       desired_count     = 1
#       spec_file         = file("${path.root}/container_spec_files/mosar-game-container.tpl")
#       subnet_ids        = module.networking.private_subnet.*.id
#       security_groups   = concat(module.networking.private_security_groups["private-mosar"].*.id, module.networking.internal_security_groups.*.id, module.networking.cidr_security_groups["dns"].*.id)
#       family            = "game-family"
#       use_load_balancer = false
#     }
#     frontend = {
#       name              = "frontend"
#       desired_count     = 1
#       spec_file         = file("${path.root}/container_spec_files/mosar-frontend-container.tpl")
#       subnet_ids        = module.networking.private_subnet.*.id
#       security_groups   = concat(module.networking.private_security_groups["private-mosar"].*.id, module.networking.internal_security_groups.*.id, module.networking.cidr_security_groups["dns"].*.id)
#       family            = "frontend-family"
#       use_load_balancer = true
#     }
#   }
# }

locals {
  secrets = {
    truststore_pass = {}
  }
}



