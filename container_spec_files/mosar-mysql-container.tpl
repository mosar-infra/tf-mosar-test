[
  {
    "name": "mysql",
    "image": "mysql/mysql-server:8.0.25",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "mosar-mysql-${environment}",
        "awslogs-group": "${log_group_name}"
      }
    },
    "portMappings": [
      {
        "containerPort": 3306,
        "hostPort": 3306,
        "protocol": "tcp"
      }
    ],
    "cpu": 1,
    "environment": [
      {
        "name": "MYSQL_DATABASE",
        "value": "${database_flashcards}"
      }
    ],
    "secrets": [
      {
        "name": "MYSQL_ROOT_PASSWORD",
        "valueFrom": "${db-root-password}"
      },
      {
        "name": "MYSQL_PASSWORD",
        "valueFrom": "${db-password}"
      },
      {
        "name": "MYSQL_USER",
        "valueFrom": "${db-username}"
      }
    ],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 2048,
    "volumesFrom": []
  }
]
