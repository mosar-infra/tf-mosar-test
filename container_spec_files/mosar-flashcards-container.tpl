[
  {
    "name": "flashcards",
    "image": "devdivision/mosar-flashcards:${image_tag}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "mosar-flashcards-${environment}-service",
        "awslogs-group": "${log_group_name}"
      }
    },
    "portMappings": [
      {
        "containerPort": 8181,
        "hostPort": 8181,
        "protocol": "tcp"
      }
    ],
    "cpu": 1,
    "environment": [
      {
        "name": "DATABASE",
        "value": "${database_flashcards}"
      },
      {
        "name": "SPRING_PROFILES_ACTIVE",
        "value": "${spring_profile}"
      },
      {
        "name": "DB_HOST",
        "value": "${db_host}"
      },
      {
        "name": "DB_USER",
        "value": "${db_user}"
      },
      {
        "name": "TRUSTSTORE_PASS",
        "value": "${truststore_pass}"
      }
    ],
    "secrets": [],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 2048,
    "volumesFrom": [],
    "command": ["bash", "start-flashcards.sh"]
  }
]
