data "aws_acm_certificate" "mosar-test" {
  domain = "mosar-test.inquisitive.nl"
}

# data "aws_db_instance" "mosar_test" {
#   db_instance_identifier = "mosar-db"
# }

data "aws_secretsmanager_secret" "db_username1" {
  arn = "arn:aws:secretsmanager:eu-central-1:640753244498:secret:db-username1-CUEiJG"
}

data "aws_secretsmanager_secret_version" "db_username1" {
  secret_id = data.aws_secretsmanager_secret.db_username1.id
}

