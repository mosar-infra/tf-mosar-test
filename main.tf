# root/main.tf

# module "networking" {
#   source               = "git::https://gitlab.com/mosar-infra/tf-module-networking.git?ref=tags/v3.0.3"
#   public_subnet_count  = 2
#   private_subnet_count = 3
#   vpc_cidr             = local.vpc_cidr
#   max_subnets          = 20
#   public_cidrs         = [for i in range(2, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
#   private_cidrs        = [for i in range(1, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
#   cidr_security_groups = local.cidr_security_groups
#   sg_security_groups   = local.sg_security_groups
#   create_natgw         = true
#   environment          = var.environment
# }

# module "loadbalancing" {
#   source                    = "git::https://gitlab.com/mosar-infra/tf-module-loadbalancing.git?ref=tags/v3.0.0"
#   lb_name                   = "mosar-ecs-frontend-lb"
#   public_subnet_ids         = module.networking.public_subnet.*.id
#   public_security_group_ids = module.networking.public_security_groups.*.id
#   vpc_id                    = module.networking.vpc_id
#   environment               = var.environment
#   listener_port             = 443
#   listener_protocol         = "HTTPS"
#   ssl_policy                = "ELBSecurityPolicy-2016-08"
#   lb_target_groups          = local.lb_target_groups
#   certificate_arn           = data.aws_acm_certificate.mosar-test.arn
#   lb_listener_target_group  = "mosar"
# }

# module "ecs" {
#   source                     = "git::https://gitlab.com/mosar-infra/tf-module-ecs.git?ref=tags/v1.0.31"
#   container_spec_definitions = local.container_spec_definitions
#   vpc_id                     = module.networking.vpc_id
#   db_user                    = data.aws_secretsmanager_secret_version.db_username1.secret_string
#   db_host                    = data.aws_db_instance.mosar_test.address
#   truststore_pass            = module.secrets.secrets_versions["truststore_pass"].secret_string
#   spring_profile             = var.spring_profile
#   database_flashcards        = "flashcards"
#   log_group_name             = "mosar-logs-${var.environment}"
#   cluster_name               = "mosar-ecs-cluster-${var.environment}"
#   image_tag                  = var.image_tag
#   ecs_task_execution_role    = module.iam.role["ecs_task_execution"]
#   ecs_task_role              = module.iam.role["ecs_task"]
#   environment                = var.environment
#   lb_target_group_arn        = module.loadbalancing.lb_tg["mosar"].arn
#   lb_container_port          = 80
#   lb_container_name          = "frontend"
# }

module "secrets" {
  source  = "git::https://gitlab.com/mosar-infra/tf-module-secrets.git?ref=tags/v2.0.4"
  secrets = local.secrets
}

# module "iam" {
#   source  = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v2.0.19"
#   iam     = local.iam
#   db_arn  = data.aws_db_instance.mosar_test.db_instance_arn
#   db_user = data.aws_secretsmanager_secret_version.db_username1.secret_string
# }

# module "logs" {
#   source         = "git::https://gitlab.com/mosar-infra/tf-module-logs.git?ref=tags/v1.0.1"
#   log_group_name = "mosar-logs-${var.environment}"
#   environment    = var.environment
# }

